from Tiroparabolic import tiro
from Tiroparabolic import tiro2

if __name__=="__main__":

    v0=10  #velocidad inicial
    a=45  #angulo inicial (Debe darse en grados)
    y0=10  #altura inicial (debe ser positiva)
    x0=0  #posicion en x inicial
    g=9.8 #gravedad debe ser positiva (el menos ya esta en las ecuaciones)
    air=0 #resistencia del aire
    t=4 #tiempo en el que quiero determinar la velocidad en x y en y
    p=25 #Profundidad del acantilado (debe ser positiva el menos ya se tiene encuenta en las ecuaciones)
    graf1='Tiro parabolico con aire'
    graf2='Tiro parabolico en un acantilado con aire'

    ti=tiro(v0,a,y0,x0,g,air,t,graf1)
    ti2=tiro2(v0,a,y0,x0,g,air,t,graf2,p)

    #llamar metodos
    print('Velocidad inicial en x: {} m, Velocidad inicial en y: {} m '.format(ti.v0x(),ti.v0y()))
    print('Velocidad en x: {} m en t= {} s, Velocidad en y : {} m en t= {} s'.format(ti.velocidadenx(),ti.t,ti.velocidadeny(),ti.t))
    print('Tiempo de vuelo: {} s'.format(ti.tiempodevuelo()))
    print('Altura maxima: {} m'.format(ti.alturamax()))
    print('Alcance maximo: {} m'.format(ti.alcancemax()))

    graf=ti.graf()

    print('Datos para el caso del acantilado:')
    print('Velocidad en x: {} m en t= {} s, Velocidad en y : {} m en t= {} s'.format(ti2.velocidadenx(),ti2.t,ti2.velocidadeny(),ti2.t))
    print('Tiempo de vuelo: {} s'.format(ti2.tiempodevuelo()))
    print('Altura maxima: {} m'.format(ti2.alturamax()))
    print('Alcance maximo: {} m'.format(ti2.alcancemax()))


    graf2=ti2.graf()