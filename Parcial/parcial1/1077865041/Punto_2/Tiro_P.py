import numpy as np
import matplotlib.pyplot as plt

#Clase para tiro parabolico con resistencia del aire
class trayectorias:
    
    #Método constructor

    def __init__(self,alpha,x0,v0,h0,g,g_x):
        self.alpha=alpha*np.pi/180
        self.x0=x0
        self.v0=v0
        self.h0=h0
        self.g=g
        self.g_x=g_x
 
    #Construcción de los metodos necesarios para describir un movimiento de tipo tiroparabólico  
    #Componentes de la velocidad inicial
    def v0x(self):
        return self.v0*round(np.cos(self.alpha),3)
    
    def v0y(self):
        return self.v0*round(np.sin(self.alpha),3)    
    #Tiempo de vuelo
    def time(self):
        return -self.v0y()/self.g

    def time_v(self):
        t1=np.linspace(0,2*self.time(),500)
        return t1
    #Posiciones en x y y
    def pos_x(self):
        return self.v0x()*self.time_v()+(1/2)*self.g_x*(self.time_v())**2
    
    def pos_y(self):
        return self.h0+self.v0y()*self.time_v()+(1/2)*self.g*(self.time_v())**2
    #Plot
    def plot_t(self):
        plt.figure(figsize=(8,6))
        plt.plot(self.pos_x(),self.pos_y(),color="r")
        plt.grid()
        plt.title("Trayectoria del tiro parabolico con una fuerza antiparalela en x")
        plt.xlabel("Eje x")
        plt.ylabel("Eje y")
        plt.savefig("tiro_parabolico_xy.png")
        
#Movimiento con una fuerza extra en una dirección beta (No necesariamente antiparalela en x)
class trayectoria_extra(trayectorias):
    def __init__(self, alpha, x0, v0, h0, g,g_x,f_extra,beta):
        self.f_extra=f_extra
        self.beta1=beta
        self.beta=self.beta1*np.pi/180
        super().__init__(alpha, x0, v0, h0, g,g_x)

    #Nuevas posiciones
    def pos_xx(self):
        return self.pos_x()+self.v0x()*self.time_v()+(1/2)*self.f_extra*round(np.cos(self.beta),3)*(self.time_v())**2
    
    def pos_yy(self):
        return self.pos_y()+ self.h0+self.v0y()*self.time_v()+(1/2)*self.f_extra*round(np.sin(self.beta),3)*(self.time_v())**2
    
    #Grfiquemos la nueva trayectoria
    def plot_t2(self):
        plt.figure(figsize=(8,6))
        plt.plot(self.pos_xx(),self.pos_yy(),color="g")
        plt.grid()
        plt.title("Trayectoria con una fuerza extra en un angulo de {}°" .format(self.beta1))
        plt.xlabel("Eje x")
        plt.ylabel("Eje y")
        plt.savefig("tiro_parabolico_fuerza_extra.png")
        plt.show()