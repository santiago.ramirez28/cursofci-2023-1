
# Importo librerías.
import numpy as np
import sympy as sym
import matplotlib.pyplot as plt

# Primera parte del punto, comparación entre método exacto y de Euler.
class solNumerica(): # Definición de clase.
    def __init__(self, a, b, n, y0, f, point=False): # Método de construcción.
        print('Inicializando clase solNumerica.')
        
        # Atributos de clase.
        self.y0 = y0 # Condición inicial y(a) = y0
        self.a = a # Intervalo en X [a,b]
        self.b = b 
        self.n = n # Número de iteraciones.
        self.f = f # EDO a resolver.
        self.point = point # Punto para evaluar función (opcional)

        self.x = [] # Arreglos para datos de X e Y.
        self.y = []

    # Métodos.
    def h(self): # Partición de intervalo.
        return (self.b - self.a)/self.n

    def X(self): # Posición X.
        if self.point: # Para un punto cualquiera dentro del intervalo.
            self.b = self.point
            self.x = np.arange(self.a, self.point, self.h())
        else:
            self.x = np.arange(self.a, self.b, self.h())

    def metodoEuler(self): # Solución EDO método Euler.
        self.X()
        self.y = [self.y0]

        for i in range(self.n):
            self.y.append(self.y[i] + self.h()*self.f(self.x[i], self.y[i])) # Euler.
        return [self.x, self.y[:-1]]
    
    def metodoExactoSympy(self): # Solución EDO método exacto, por sympy.
        x = sym.Symbol('x') # Símbolo de variable X.
        y = sym.Function('y') # Símbolo de función Y.
        C1 = sym.Symbol('C1') # Símbolo de constante de integración.
        
        fsym = self.f(x,y(x)) # Función diferencial.
        eqf = sym.Eq(y(x).diff(x),fsym) # Ecuación simbólica.
        ci = {y(self.a):self.y0} # Condiciones iniciales.
        sed = sym.dsolve(y(x).diff(x) - fsym) # Solución EDO.
        edci = sym.Eq(sed.lhs.subs(y(x), self.y0).subs(ci), sed.rhs.subs(x, self.a)) # Evaluando CI.
        edcte = sym.solve(edci) # Reemplaza valor de cte.
        soledo = sed.subs({C1: edcte[0]}) # Reemplaza constante en EDO.
        soledonp = sym.lambdify(x, soledo.rhs, "numpy") # Transforma la ecuación a formato de numpy.
        
        self.X() # Mpetodo para arreglo de X.
        
        soledonpx = soledonp(self.x) # Evaluando arreglo solución en X.
        return [self.x, soledonpx]

    def figCompResEDO(self, titl, name): # Método que grafica las soluciones de la EDO con los dos métodos.
        plt.figure(figsize=(10,8))
        euler = self.metodoEuler()
        sympy = self.metodoExactoSympy()
        plt.plot(euler[0], euler[1], label='Euler')
        plt.plot(sympy[0], sympy[1], label='Exacto')
        plt.title(titl)
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.grid()
        plt.legend()
        plt.savefig(name)
        
# Segunda parte del punto, péndulo simple.

class penduloSimple(solNumerica): # Definición de clase, con herencia.
    def __init__(self, a, b, n, y0, f, theta0, wn, edo2): # Método constructor.
        print('Inicializando clase penduloSimple')
        
        super().__init__(a, b, n, y0, f) # Atributos de clase anterior.
        self.y0 = self.s0  = y0# Polimorfismo condición inicial en S0, nueva variable.
        self.theta0 = theta0 # Condición inicial para theta = s1.
        self.wn = wn # Frecuencia natural del péndulo (sqrt(g/l)).
        self.edo2 = edo2 # Segunda EDO resultante del cambio de variable.
        self.theta= [] # Arreglo para S1.
        
    # Métodos.
        
    def X(self): # Arreglo de tiempo, polimorfismo.
        self.x = np.arange(self.a, self.b + self.n, self.n)
    
    def metodoEuler(self): # Método de Euler, polimorfismo.
        self.theta = [self.theta0] # CI de theta.
        self.y = [self.y0] # CI de S0, en este caso y.
        
        self.X() # Arreglo del tiempo.
        
        # Como es un sistema de dos EDO, entonces Euler se redefine:
        for i in range(len(self.x) - 1):
            self.theta.append(self.theta[i] + self.n*self.f(self.y[i]))
            self.y.append(self.y[i] + self.n*self.edo2(self.theta[i + 1]))

        return self.x, self.theta, self.y
    
    def metodoExactoSympy(self): # Polimorfismo de método exacto para sistema de EDO.
        s0 = sym.Symbol('s0') # Símbolo de variable nueva S0.
        t=sym.Symbol("t") # Símbolo de variable tiempo t.
        theta = sym.Function('theta') # Símbolo de función theta.
        C1 = sym.Symbol('C1') # Símbolo de constante de integración 1.
        C2=sym.Symbol("C2") # Símbolo de constante de integración 2.
        
        fsym = -(s0**2)*theta(t) # Función diferencial.
        eqf = sym.Eq(theta(t).diff(t, t),fsym) # Ecuación simbólica.
        sed = sym.dsolve(theta(t).diff(t, t) - fsym).rhs # Solución EDO.
        edci1 = sym.Eq(sed.subs(t, 0), self.theta0) # Evaluando CI1. Ingreso condiciones a mano.
        edci2 = sym.Eq(sed.diff(t).subs(t, 0) ,0) # Evaluando CI2.
        edcte = sym.solve([edci1, edci2], (C1, C2)) # Reemplaza valor de cte.
        soledo = sed.subs(edcte) # Reemplaza constante en EDO.
        soledonp = sym.lambdify([t,s0], soledo, "numpy") # Transforma la ecuación a formato de numpy.
        
        self.X() # Mpetodo para arreglo de tiempo.
        
        soledonptheta = soledonp(self.x, self.wn) # Evaluando arreglo solución en theta.
        return [self.x, soledonptheta]
    
    def DesplazamientoAngular(self, titl, name): # Solución de desplazamiento angular.
        self.figCompResEDO(titl, name)