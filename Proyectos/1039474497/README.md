    INSTRUCCIONES
    
    1) Cargar codigo al arduino
    2) Configurar el puerto asignado al Arduino en el archivo execution_main.py y ejercutarlo
    **Se abre la ventana de la GUI**
    3) Escribir los valores del umbral superior y oprimir el botón para configurar el PHA
    4) Escribir los valores del umbral inferior y oprimir el botón para configurar el PHA
    5) Escribir el valor de ventana de tiempo
    
    ************************** serialConnection.py********************************************
    
    La clase ArduinoConnection tiene cuatro métodos principales: connect, disconnect, send y read.

    El método __init__ se ejecuta automáticamente cuando se crea un objeto de la clase. Este método inicializa los atributos port y baudrate con los valores proporcionados por el usuario. También crea un atributo connection que se utilizará para almacenar la conexión serial.

    El método connect se encarga de establecer la conexión serial con el puerto especificado en el atributo port y la velocidad de transmisión especificada en el atributo baudrate. Devuelve True si la conexión se establece correctamente y False si hay algún problema.

    El método disconnect se encarga de cerrar la conexión serial si está abierta. Devuelve True si la conexión se cerró correctamente y False si no se pudo cerrar la conexión (porque no estaba abierta).

    El método send se encarga de enviar datos a través de la conexión serial. El parámetro data debe ser una cadena de texto que se convertirá en bytes antes de enviarse. Devuelve True si los datos se enviaron correctamente y False si no se pudo enviar los datos (porque no había conexión abierta).

    El método read se encarga de leer datos desde la conexión serial. Devuelve una cadena de texto con los datos leídos, sin incluir el carácter de nueva línea (\n). Si no se pudo leer nada (porque no había conexión abierta o no había datos disponibles), devuelve None.


    ************************** Ejecución execution_serial.py *******************************

    Se crea un objeto de la clase ArduinoConnection, pasando el puerto serial y la velocidad de transmisión como parámetros.

    Se llama al método connect para establecer la conexión con Arduino

    Una vez que la conexión se establece correctamente, se pueden enviar y recibir datos utilizando los métodos send y read, respectivamente.

    *************************** Funcionamiento general **************************************

    Al oprimir el boton Th. Up - Th. Low, la clase GUI llama a la clase serialConnection para enviar los parámetros de configuración Threshold up y low. En el programa de Arduino, estos datos se reconocen por sus iniciales 'TU' y 'TL'. Allí se toman sus valores y se asigan a las variables de los umbrales superior e inferior