import tkinter as tk
import numpy as np
from tkinter import messagebox
from serialConnection import ArduinoConnection
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg




class GUI:
    def __init__(self, port, speed):
        self.port = port
        self.speed = speed
        self.root = tk.Tk()
        self.root.title("PHA UdeA 1.0.0")
        self.label = tk.Label(self.root, text="Estado: Sin errores") # Crear etiqueta para mostrar el estado
        self.label.grid(row=0, column=0, padx=10, pady=10) # Use grid() to place the label in row 0, column 0
        self.root.geometry("740x450")  # Establecer el tamaño de la ventana
        self.create_widgets()

    #************************************* Apariencia **************************************************
    def create_widgets(self):
        # Crear botón A y posicionarlo en la fila 1, columna 0
        button_a = tk.Button(self.root, text="Gen PHA", command=self.button_a_clicked)
        button_a.grid(row=1, column=0, padx=5, pady=1)  # Posicionar en la fila 1, columna 0

        # Crear botón B y posicionarlo en la fila 2, columna 0
        button_b = tk.Button(self.root, text="Th. Up", command=self.button_b_clicked)
        button_b.grid(row=2, column=0, padx=5, pady=1, sticky="e")  # Posicionar en la fila 2, columna 0

        self.entry_b = tk.Entry(self.root)
        self.entry_b.grid(row=2, column=1, padx=5, pady=1)  # Posicionar en la fila 2, columna 1
        # Agregar texto "mV" a la derecha del campo de texto B
        label_b = tk.Label(self.root, text="mV")
        label_b.grid(row=2, column=2, padx=5, pady=1, sticky="w")

        # Crear botón C y posicionarlo en la fila 3, columna 0
        button_c = tk.Button(self.root, text="Th. Low", command=self.button_c_clicked)
        button_c.grid(row=3, column=0, padx=5, pady=1, sticky="e")  # Posicionar en la fila 3, columna 0

        # Crear campo de texto junto al botón c y posicionarlo en la fila 3, columna 1
        self.entry_c = tk.Entry(self.root)
        self.entry_c.grid(row=3, column=1, padx=5, pady=1, sticky="w")  # Posicionar en la fila 3, columna 1
        # Agregar texto "mV" a la derecha del campo de texto C
        label_c = tk.Label(self.root, text="mV")
        label_c.grid(row=3, column=2, padx=5, pady=1, sticky="w")

        # Crear botón D y posicionarlo en la fila 4, columna 0
        button_d = tk.Button(self.root, text="Time Window", command=self.button_d_clicked)
        button_d.grid(row=4, column=0, padx=5, pady=1, sticky="e")  # Posicionar en la fila 4, columna 0

        # Crear campo de texto junto al botón D y posicionarlo en la fila 4, columna 1
        self.entry_d = tk.Entry(self.root)
        self.entry_d.grid(row=4, column=1, padx=5, pady=1, sticky="w")
        # Agregar texto "ms" a la derecha del campo de texto D
        label_d = tk.Label(self.root, text="ms")
        label_d.grid(row=4, column=2, padx=5, pady=1, sticky="w")


        # Crear botón E y posicionarlo en la fila 5, columna 0
        button_e = tk.Button(self.root, text="Sampling time", command=self.button_e_clicked)
        button_e.grid(row=5, column=0, padx=5, pady=1, sticky="e")  # Posicionar en la fila 4, columna 0

        # Crear campo de texto junto al botón E y posicionarlo en la fila 5, columna 1
        self.entry_e = tk.Entry(self.root)
        self.entry_e.grid(row=5, column=1, padx=5, pady=1) 
        # Agregar texto "s" a la derecha del campo de texto E
        label_e = tk.Label(self.root, text="s")
        label_e.grid(row=5, column=2, padx=5, pady=1, sticky="w")

        for i in range(0,6):
            self.root.grid_rowconfigure(i, minsize=0)

         # Llamar a la función plot_gaussian() para crear el gráfico de la gaussiana
        self.plot_gaussian()

#************************************* Funciones **************************************************

    #funcion generalizada para enviar el dato del campo de texto con el boton correspondiente
    def send_to_arduino(self, prefix, data):
        # Crea una instancia de la clase ArduinoConnection
        arduino = ArduinoConnection(self.port,self.speed)

        try:
            if arduino.connect():
                self.label.config(text="Estado: Conexión establecida correctamente") # Actualizar etiqueta con el estado
        except Exception as e:
                self.label.config(text="Error de conexión") # Actualizar etiqueta con el estado de error

        arduino.send(prefix + str(data)) # Envía los datos al arduino
        respuesta = arduino.read() # Recibe los datos del arduino
        print(respuesta)
        arduino.disconnect()

 #Botones para configurar parametros del PHA  
  
    def button_a_clicked(self):
        messagebox.showinfo("Botón A", "Haz clic en el botón A")

    def button_b_clicked(self):
        # Obtener el texto ingresado en el campo de texto
        texto = self.entry_b.get()
        try:
            numero = int(texto)
            self.send_to_arduino('TU', numero) # Llama al método send_to_arduino con el prefijo 'TU'
        except ValueError:
            messagebox.showerror("Error", "Ingresa un número válido en el campo de texto")

    def button_c_clicked(self):
        # Obtener el texto ingresado en el campo de texto
        texto = self.entry_c.get()
        try:
            numero = int(texto)
            self.send_to_arduino('TL', numero) # Llama al método send_to_arduino con el prefijo 'TL'
        except ValueError:
            messagebox.showerror("Error", "Ingresa un número válido en el campo de texto")

    def button_d_clicked(self):
        # Obtener el texto ingresado en el campo de texto
        texto = self.entry_d.get()
        try:
            numero = int(texto)
            self.send_to_arduino('TW', numero) # Llama al método send_to_arduino con el prefijo 'TW'
        except ValueError:
            messagebox.showerror("Error", "Ingresa un número válido en el campo de texto")

    def button_e_clicked(self):
        # Obtener el texto ingresado en el campo de texto
        texto = self.entry_e.get()
        try:
            numero = int(texto)
            self.send_to_arduino('TM', numero) # Llama al método send_to_arduino con el prefijo 'TM'
        except ValueError:
            messagebox.showerror("Error", "Ingresa un número válido en el campo de texto")

#************************************************** Grafica ****************************************************

    def plot_gaussian(self):
        # Parámetros de la gaussiana
        mu = 1.6
        sigma = 1

        # Crear datos de la gaussiana
        x = np.linspace(0, 3.3, num=100)
        y = (1/(sigma * np.sqrt(2 * np.pi))) * np.exp(-0.5 * ((x - mu)/sigma)**2)

        # Crear la figura y el gráfico
        fig = plt.figure(figsize=(5, 4), dpi=80)
        ax = fig.add_subplot(111)
        ax.plot(x, y, '.')
        ax.set_xlabel('Channel (mV)')
        ax.set_ylabel('Counts')
        ax.set_title('Espectro de alturas')

        # Crear el lienzo del gráfico
        canvas = FigureCanvasTkAgg(fig, master=self.root)
        canvas.draw()

        # Colocar el lienzo en la interfaz gráfica
        canvas.get_tk_widget().grid(row=1, column=3, rowspan=4, padx=10, pady=10)




    def start(self):
        self.root.mainloop()





