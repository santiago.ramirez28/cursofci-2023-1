import numpy as np
import matplotlib.pyplot as plt 
import mpl_toolkits


class movcampmagnetico():
    # Constructor
    def __init__(self, Ek, theta, mass, charge, magfield):
        self.Ek = Ek
        self.theta = theta
        self.mass = mass
        self.charge = charge
        self.magfield = magfield
    
    # Calcula la magnitud de la velocidad en m/s
    def v(self):
        try: 
            v = np.sqrt(2*self.Ek*9e16/(self.mass))
            return v 
        except:
            raise Exception("Valor de energia cinetica no valido.")
        
    # Calcula las componentes x y z de la velocidad
    def v0x(self):
        return self.v() * np.sin(np.pi/180 * self.theta)
    
    def v0z(self):
        return self.v() * np.cos(np.pi/180 * self.theta)
    
    # Calcula el radio de curvatura de la helice del movimiento
    def r(self):
        r = self.mass * self.v0x() * 9.38e-16  / (np.abs(self.charge) * self.magfield)
        return r
    
    # Calcula la frecuencia angular 
    def w(self):
        w = np.abs(self.charge) * 9e16 * self.magfield / self.mass
        return w

    # Calcula los arreglos para las posiciones x, y, z
    # Se toma un tiempo de 1 microsegundo dividido en 10000 pasos

    def arrX(self):
        t = np.arange(0, 100, 0.01)
        # Condicional para movimiento rectilineo
        if self.charge == 0 or self.magfield ==0:
            arrX = np.round(self.v0x() * t * 1.0e-8 , 6)
            return arrX
        r = self.r()
        w = self.w() 
        arrX = np.round(r * np.sin(w * t * 1.0e-8), 6)
        return arrX
    
    def arrY(self):
        t = np.arange(0, 100, 0.01)
        # Condicional para movimiento rectilineo
        if self.charge == 0 or self.magfield ==0:
            arrX = np.round( 0 * t * 1.0e-8 , 6)
            return arrX
        r = self.r()
        w = self.w() 
        arrY = np.round(np.sign(self.charge) * r * np.cos(w * t * 1.0e-8), 6)
        return arrY
    
    def arrZ(self):
        t = np.arange(0, 100, 0.01)
        arrZ = np.round(self.v0z() * t * 1.0e-8 , 3)
        return arrZ

    # Metodo que genera la grafica en 3D de la trayectoria
    def graph(self):
        x = self.arrX()
        y = self.arrY()
        z = self.arrZ()

        fig = plt.figure(figsize=(16,16))
        ax = plt.axes(projection="3d")
        ax.plot3D(x, y, z, 'black', label= "Trayectoria")
        ax.scatter(x[0], y[0], z[0], 'blue', label= "Punto inicial")
        ax.scatter(x[-1], y[-1], z[-1], 'red', label= "Punto final")
        ax.set_xlabel(r"$x \; (m)$", fontsize=15)
        ax.set_ylabel(r"$y \; (m)$", fontsize=15)
        ax.set_zlabel(r"$z \; (m)$", fontsize=15)
        plt.legend( fontsize= 20 )
        plt.savefig("trayectoria.png")
