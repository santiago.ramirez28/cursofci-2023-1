#Código para calcular la trayectoria helicoidal de una particula en un campo

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#Definiendo la clase helicoidal
class helicoidal():
    def __init__(self,tiempo,energia,masa,carga,campo,angulo):
        self.t = tiempo #tiempo de vuelo de la partiucla
        self.E = energia #energia de la particular
        self.m = masa #masa de la particula
        self.q = carga #carga de la particula
        self.B = campo #campo magnético
        self.ang = angulo #angulo de entrada
        self.v = np.sqrt(self.E*2/self.m) #velocidad inicial
        self.vxy = self.v*np.sin(np.pi/180*self.ang) #velocidad en x y y perpendicular al campo
        self.R = self.vxy*self.m/(abs(self.q)*self.B) #radio de la trayectoria
        self.w = abs(self.q)*self.B/self.m #frecuencia angular

    def motionx(self):
        x = self.R*np.sin(self.w*self.t) #movimiento en x
        return x

    def motiony(self):
        y = self.R*np.cos(self.w*self.t) #movimiento en y
        return y
    
    def motionz(self):
        vz = self.v*np.cos(np.pi/180*self.ang) #velocidad en z
        z = vz*self.t #movimiento en z
        return z
    
    def period(self):
        T = 2*np.pi/self.w #periodo de la trayectoria
        return T
    
    def motiongraf(self):
        # Creamos la figura
        fig = plt.figure()
        # Agrrgamos un plano 3D
        ax1 = fig.add_subplot(111,projection='3d')
        ax1.plot(self.motionx(),self.motiony(),self.motionz(),"r",label="Trayectoria con B = {} T en z".format(self.B))
        ax1.set_xlabel('X [m]')
        ax1.set_ylabel('Y [m]')
        ax1.set_zlabel('Z [m]')
        ax1.set_title('Trayectoria helicoidal')
        ax1.legend()
        plt.savefig("Trayectoria helicoidal.png")
        # Mostramos el gráfico
        plt.show()
        return
        






