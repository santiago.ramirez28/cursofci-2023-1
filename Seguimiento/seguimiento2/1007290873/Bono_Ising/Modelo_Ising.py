import numpy as np
import matplotlib.pyplot as plt

"""
Script para implementar el modelo de Ising 2D con Monte Carlo, y graficar el comportamiento del
calor específico en función de la temperatura
"""

# Fijamos una semilla para los valores aleatorios
np.random.seed(5)


# Creamos el arreglo de temperatura a partir del cual graficaremos Cv vs T
size_T = 300
Temp_array = np.linspace(0.7, 6, size_T)
# Generamos un arreglo de ceros para el calor específico, que después se actualizará
Specific_heat_array = np.zeros(len(Temp_array))

# Definimos el número de iteraciones que llevaremos a cabo
N = 1000

# Creamos un arreglo que contendra los 4 valores de spin, de manera aleatoria
S_array = [np.random.choice([1,-1]) for i in range(4)]

# De acuerdo con la ayuda de clase, generamos un diccionario que contiene 
# las posiciones de los vecinos cercanos para cada partícula
near_particles={n : ( (n // 2) * 2 + (n + 1) % 2, (n + 2) % 4,\
                     (n // 2) * 2 + (n - 1) % 2, (n - 2) % 4) for n in range(4)}

# Definimos función para calcular la probablidad de que el spin de cada partícula sea +1
prob_spin_up = lambda h, beta, sigma: 1/(1 + np.exp(-beta * sigma* h))

# Ahora iteramos sobre todas las temperaturas
i = 0
for Ti in Temp_array:

    # Constante que aparece en la probabilidad de cambiar el spin a 1
    # y se actualiza con cada valor de temperatura
    beta = 1/Ti
    sigma = 2

    # Inicializamos en cero el valor de energía
    Energy_val = 0
    # Creamos un arreglo vacío para almacenar cada valor
    Energy_array = []
    
    for j in range(N):
        
        # Generamos un número entero aleatorio entre 0 y 2 
        rand_pos = np.random.randint(0, 3)

        # Almacenamos el valor del espín en esta posición aleatoria
        original_S_value = S_array[rand_pos]

        # Modificamos el valor de este espín y lo fijamos como -1
        S_array[rand_pos] = -1

        # Generamos el valor de corte para la probabilidad
        corte = np.random.rand(1)[0]

        # Calculamos el valor de h, como la suma de los espines de las partículas que son cercanas
        h = sum(S_array[position] for position in near_particles[rand_pos])

        # Verificamos si la partícula debe cambiar de valor de spin
        # usando la función de probabilidad y el valor de corte
        if corte < prob_spin_up(h, beta, sigma):

            S_array[rand_pos] = 1

        if S_array[rand_pos] != original_S_value:

            Energy_val -= 2 * S_array[rand_pos] * h

        Energy_array.append(Energy_val)

    mean_Energy = np.mean(np.array(Energy_array))
    mean_Energy2 = np.mean(np.array(Energy_array)**2)

    Specific_heat_value = (beta**2 / 4) * (mean_Energy2 - mean_Energy ** 2)
    Specific_heat_array[i] = Specific_heat_value

    i += 1  

   
# Graficamos ahora calor específico vs. temperatura
plt.scatter(Temp_array, Specific_heat_array)
plt.xlabel("T")
plt.ylabel("Cv / N")
plt.savefig("Cv_vs_T.png")