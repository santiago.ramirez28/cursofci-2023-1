# Punto 2: Distribución de velocidades de Maxwell-Boltzmann. Ejecución.
# Problema de la molécula de aire.

import P2ClassMaxwell
import numpy as np

if __name__=='__main__': # Llamo la ejecución.
  # Defino atributos.
  m = 4.82e-26 # Masa del aire [kg].
  r = 2.0e-10 # Radio de molécula de aire [m].
  T = 300 # Temperatura absoluta del sistema [K].
  P = 1e5 # Presión del sistema [Pa].
  a = 0 # Límite inferior de la integral.
  b = 1e3 # Límite superior de la integral (infinito).
  N = 10e3 # Número de iteraciones.

  sol = maxwellBoltzmann(a, b, N, m, r, T, P) # Instanciando la clase.

  sol.grafVelMed() # Gráfica velocidad media.

  sol.grafCaminoLibre() # Gráfica Camino libre medio.

  solana1, solana2 = sol.metExacto()

  solmonte1, solmonte2 = sol.monteCarlo()

  # Imprimiendo los valores por cada método.
  print("La velocidad media de las moléculas de aire es, por el método exacto,\
  aproximadamente: ", solana1[0])

  print("La velocidad media de las moléculas de aire es, por el método de\
  Monte Carlo, aproximadamente: ", solmonte1)

  print('\n -------------------------------------------------- \n')

  print("El camino libre medio de las moléculas de aire es, por el método exacto,\
  aproximadamente: ", solana2[0])

  print("El camino libre medio de las moléculas de aire es, por el método de\
  Monte Carlo, aproximadamente: ", solmonte2)