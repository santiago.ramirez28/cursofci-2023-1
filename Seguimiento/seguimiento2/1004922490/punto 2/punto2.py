import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad

class Integral:
  def __init__(self,N,n,T,V_i,V_f,R):
    # Definir parámetros del sistema
    self.N=N
    self.n=n #cantidad de moles de gas
    self.T=T #temperatura en k
    self.V_i=V_i #volumen inicial en L
    self.V_f=V_f #volumen final en L
    self.R=R # constante de los gases ideales en J/(mol K)

   
  # Definimos la función a integrar
  def f(x):
      return 1/x
  
  def valores_integral(self):
    def f(x):
      return 1/x
    #lista vacia para llenar con los valores 
    valores =[]
    for i in range(self.N):
      self.N=10*(i+1)
  # Generamos N puntos aleatorios en el intervalo [a, b]
      x = np.random.uniform(self.V_i, self.V_f, self.N)
  # Evaluamos la función en los puntos generados
      
  # Calculamos la aproximación de la integral utilizando el método de Monte Carlo
      I_mc = (self.V_f-self.V_i) * np.mean(f(x))
      valores.append(I_mc)#introducir los valores en la lista vacia
    return valores
  
   # Calculamos la integral exacta utilizando scipy.integrate.quad
  def integral_real(self):
    def f(x):
      return 1/x
    I_exact, _ = quad(f, self.V_i, self.V_f)
    return  I_exact



