import numpy as np
import matplotlib.pyplot as plt

class Decaimiento():
    def __init__(self,dt,N_Ra,N_Ac,T_f,λ_Ra,λ_Ac):
        
        self.dt=dt      # paso de tiempo
        self.T_f=T_f        # tiempo final
        self.N=N_Ra         # número de atomos iniciales
        self.N_Ra=N_Ra       # número de átomos del Radio 255
        self.N_Ac=N_Ac      # número de átomos del Actinio 255
        self.λ_Ra=λ_Ra      # tiempo de vida medio del Radio 255
        self.λ_Ac=λ_Ac      # número inicial de átomos del Actinio 255
        self.t=np.arange(0,self.T_f,self.dt)     # arreglo de tiempos para la gráfica
        self.prob_Ra=1-np.exp(-self.λ_Ra*self.dt)   # probabilidad de que decaiga el Ra
        self.prob_Ac=1-np.exp(-self.λ_Ac*self.dt)   # probabilidad de que decaiga el Ac
    
    def conteo(self):
        List_Ra=[]
        List_Ac=[]

        for j in range(len(self.t)):
            sigma=np.random.uniform(0,1,self.N_Ra)      # tantos números aleatorios como # de atomos inicial
            mask=self.prob_Ra>sigma 
            Desintegracion=np.where(mask==True,1,0)   # si la probabilidad que decaiga Ra es mayor que el número aleatorio
                                                        # se pone un True y el átomo decae, sino un False
            self.N_Ra=self.N_Ra-Desintegracion.sum()
            self.N_Ac=self.N_Ac+Desintegracion.sum()

            List_Ra.append(self.N_Ra)

            sigma2=np.random.uniform(0,1,np.abs(self.N_Ac))      # números aleatorios
            mask2=self.prob_Ac>sigma2
            Desintegracion2=np.where(mask2==True,1,0)

            self.N_Ac=self.N_Ac-Desintegracion2.sum()

            List_Ac.append(self.N_Ac)

        return List_Ra, List_Ac
        
    def Plot(self):
        Ra,Ac=self.conteo()
        plt.title("Decaimiento de $^{255}Ra$ y $^{255}Ac$")
        plt.plot(self.t,self.N*np.exp(-self.t*self.λ_Ra),"black",label="$^{255}Ra$ Analítica")
        plt.scatter(self.t,Ra,0.4,"red",label="$^{255}Ra$ Montecarlo")


        plt.plot(self.t,self.λ_Ra/(self.λ_Ac-self.λ_Ra)*self.N*(np.exp(-self.t*self.λ_Ra) \
        -np.exp(-self.t*self.λ_Ac)),"blue",label="$^{255}Ac$ Analítica")

        plt.scatter(self.t,Ac,0.4,"orange",label="$^{255}Ac$ Montecarlo")
        plt.legend()
        plt.grid()
        plt.show()